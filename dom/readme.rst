
Praca domowa #6
===============

Zaimplementować aplikację do mikroblogowania w innym niż Flask małym frameworku webowym w języku Python.
Aplikacja powinna:

* korzystać z bazy danych (np. sqlite)
* wyświetlać wpisy
* umożliwiać dodawanie wpisów dla zalogowanego użytkownika, a dla innego nie
* autentykować użytkowników
* wyświetlać informacje o błędach i sukcesach
* schludnie wyglądać
* działać w każdym przypadku